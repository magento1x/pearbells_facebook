# Pearbells_Facebook

https://pearlbells.co.uk/facebook-pixel-magento-extension/

Facebook Pixel Magento Extension
Posted on October 22, 2017 by pearl - Magento Extension

This is a free facebook pixel magento extension.The extension add facebook pixel code to the magento website.It also provides to option set advanced facebook pixel from magento backend.


Installation:

- Download the zip file
- Extract zip and copy files to magento root folder
- Login to magento admin (System->Configuration) and do the following settings

Facebook Pixel Settings

![](https://gitlab.com/magento1x/pearbells_facebook/-/raw/main/facebook_pixel_magento_extension.png)
![](https://gitlab.com/magento1x/pearbells_facebook/-/raw/main/advanced_facebook_pixel_magento_extension.png)

Advanced Facebook Pixel Magento Extension

Live Testing
The chrome extension facebook pixel helper is a good tool to test facebook pixel installation.

Facebook Pixel Magento Extension
Click here to [download](https://pearlbells.co.uk/wp-content/uploads/2017/10/Pearlbells_Facebookpixel.zip) the extensions and also refer other [magento extensions](https://www.pearlbells.co.uk/category/magento-extension/).
